# frozen_string_literal: true

module StubEnvironment
  def stub_environment(variables = {})
    allow(ENV).to receive(:[]).and_call_original

    variables.each do |key, value|
      allow(ENV).to receive(:[]).with(key.to_s.upcase).and_return(value.to_s)
    end
  end
end
