lib = File.join(__dir__, '..', 'lib')
$LOAD_PATH.unshift(lib)

require 'bot'

Dir['./spec/support/**/*.rb'].sort.each { |f| require f }

RSpec.configure do |config|
  config.include(NextInstanceOf)
  config.include(StubEnvironment)

  config.after { ::Bot.reset_environment }
end
