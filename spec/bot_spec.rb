# frozen_string_literal: true

RSpec.describe Bot do
  describe '.bot' do
    let(:discord_token) { 'rspec tests token' }

    subject(:bot) { described_class.bot }

    before do
      stub_environment(discord_token: discord_token)
    end

    it 'returns a discordrb bot using the token from the environment' do
      expect(bot).to be_a(Discordrb::Bot)
      expect(bot).to have_attributes(token: "Bot #{discord_token}")
    end
  end

  describe '.environment' do
    let(:discord_token) { 'rspec tests token' }
    let(:discord_server_id) { 1234 }

    subject(:environment) { described_class.environment }

    let(:attributes) do
      {
        discord_token: discord_token,
        discord_server_id: discord_server_id
      }
    end
    
    before do
      stub_environment(**attributes)
    end

    it 'returns the loaded environment' do
      expect(environment).to have_attributes(**attributes)
    end
  end

  describe '.server' do
    let(:bot) { instance_double(Discordrb::Bot) }
    let(:discord_server_id) { 1234 }
    let(:stubbed_server) { instance_double(Discordrb::Server) }

    subject(:server) { described_class.server }

    before do
      allow(described_class).to receive(:bot).and_return(bot)
      stub_environment(discord_token: 'token', discord_server_id: discord_server_id)
    end

    context 'when bot is connected to expected server' do
      before do
        allow(bot).to receive(:servers).and_return({ discord_server_id => stubbed_server })
      end

      it 'returns the server' do
        expect(server).to eq(stubbed_server)
      end
    end

    context 'when bot is connected to unknown server' do
      before do
        allow(bot).to receive(:servers).and_return({ 4321 => stubbed_server })
      end

      it { is_expected.to be_nil }
    end

    context 'when bot is not connected to server' do
      before do
        allow(bot).to receive(:servers).and_return({})
      end

      it { is_expected.to be_nil }
    end
  end
end
