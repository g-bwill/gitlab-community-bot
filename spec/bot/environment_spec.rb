# frozen_string_literal: true

RSpec.describe Bot::Environment do
  describe '.load!' do
    subject(:load!) { described_class.load! }

    before do
      ENV['DISCORD_TOKEN'] = 'discord_token_value'
    end

    context 'when all required environment variables are present' do
      it { is_expected.to have_attributes(discord_token: 'discord_token_value') }
    end

    context 'when required environment variables are missing' do
      before do
        ENV['DISCORD_TOKEN'] = nil
      end

      it { expect { load! }.to raise_error(ArgumentError) }
    end

    context 'when discord_server_id is present' do
      before do
        ENV['DISCORD_SERVER_ID'] = '1234'
      end

      it { expect(load!).to have_attributes(discord_server_id: 1234) }
    end

    context 'when discord_server_id is not present' do
      before do
        ENV['DISCORD_SERVER_ID'] = nil
      end

      it { expect(load!).to have_attributes(discord_server_id: described_class::PRODUCTION_DISCORD_SERVER) }
    end
  end
end
