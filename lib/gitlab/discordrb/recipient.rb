# frozen_string_literal: true

module Gitlab
  module Discordrb
    # In private channels and DMs, the server and role information is not available.
    # Since this is a single-server bot, this module overrides `Discordrb::Recipient`
    # to provide this information from the GitLab Discord server.
    module Recipient
      def server
        ::Bot.server
      end

      def member
        @member ||= server&.member(id)
      end

      def roles
        member.roles
      end
    end
  end
end

Discordrb::Recipient.prepend(Gitlab::Discordrb::Recipient)
