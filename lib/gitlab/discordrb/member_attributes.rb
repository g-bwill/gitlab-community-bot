# frozen_string_literal: true

module Gitlab
  module Discordrb
    module MemberAttributes
      def gitlab_staff?
        roles.any? { |role| role.name == 'GitLab Staff' }
      end
    end
  end
end

Discordrb::MemberAttributes.prepend(Gitlab::Discordrb::MemberAttributes)
