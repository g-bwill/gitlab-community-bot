# frozen_string_literal: true

module Bot
  module Reactive
    def self.load_hooks(bot)
      all_hooks.each do |hook|
        puts "Loading reactive hook: #{hook}"
        bot.include!(hook)
      end
    end

    private

    def self.all_hooks
      ::Bot::Reactive.constants.filter_map do |c|
        const = const_get(c)

        const if const.is_a?(::Discordrb::EventContainer)
      end
    end
  end
end
