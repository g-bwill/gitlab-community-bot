# frozen_string_literal: true

module Bot
  class Environment
    PRODUCTION_DISCORD_SERVER = 778180511088640070.freeze
    VARIABLES = [
      {
        name: :discord_token,
        required: true
      },
      {
        name: :discord_server_id,
        required: false,
        default: PRODUCTION_DISCORD_SERVER,
        coerce_with: :to_i
      }
    ].freeze

    attr_reader :errors

    def initialize
      @errors = []
    end

    def self.load!
      environment = new
      environment.load!

      raise ArgumentError, environment.errors.join(', ') if environment.errors.present?

      environment
    end
    
    def self.all_variable_names
      VARIABLES.map { |variable| variable[:name] }
    end
    private_class_method :all_variable_names

    attr_reader *all_variable_names

    def load!
      VARIABLES.each do |variable|
        name = variable[:name]
        env_name = name.to_s.upcase
        value = ENV[env_name]

        if value.blank?
          if variable[:required]
            errors.push("Required variable #{env_name} is not present")
            next
          else
            value = variable[:default]
          end
        end

        if (coerce_with = variable[:coerce_with]).present?
          value = value.public_send(coerce_with)
        end

        send("#{name}=", value)
      end
    end

    private

    attr_writer *all_variable_names
  end
end
