# frozen_string_literal: true

module Bot
  module Reactive
    module MemberJoin
      extend ::Discordrb::EventContainer

      JOIN_MESSAGE = <<~MESSAGE
        Welcome to the GitLab Community!

        If you are interested in contributing, have a look at
        https://about.gitlab.com/community/contribute/ and join the discussion in #contribute.
      MESSAGE

      member_join do |event|
        return if event.user.bot_account?

        event.respond(JOIN_MESSAGE)
      end
    end
  end
end
