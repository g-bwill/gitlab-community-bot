# frozen_string_literal: true

require 'socket'

module Bot
  module Reactive
    module Debug
      extend ::Discordrb::EventContainer

      ready { puts 'Bot is ready!'; $stdout.flush }

      message(content: 'debug') do |event|
        message = <<~MESSAGE
        Current time: `#{Time.now}`
        Server: `#{server_info(event)}`
        Your roles: `#{event.user.roles.map(&:name)}`
        MESSAGE

        message += "Hostname: `#{Socket.gethostname}`" if event.user.gitlab_staff?
        event.respond(message)
      end

      def self.server_info(event)
        server = event.user.server

        return 'none' unless server

        "#{server.name} (#{server.id})"
      end
    end
  end
end
