# frozen_string_literal: true

require 'active_support'
require 'discordrb'
require 'zeitwerk'

loader = Zeitwerk::Loader.for_gem(warn_on_extra_files: false)
loader.setup

module Bot
  def self.run
    ::Bot::Reactive.load_hooks(bot)

    bot.run
  rescue Interrupt
    puts "\nBye!"
    $stdout.flush
  end

  def self.bot
    @bot ||= Discordrb::Bot.new(token: environment.discord_token)
  end

  def self.environment
    @environment ||= ::Bot::Environment.load!
  end

  def self.reset_environment
    return unless instance_variable_defined?(:@environment)

    remove_instance_variable(:@environment)
  end

  def self.server
    bot.servers[environment.discord_server_id]
  end
end

loader.eager_load
